# Influence of Geant3 and Geant4 on MFT Tracks

Project to compare Geant3 and Geant4 differences in O2 simulation, focusing on MFT reconstruction tracks.

## Files

* `generate_g3g4.sh`: script to run 2 simulations, each with one simulator, and analize data with macro.
* `g3g4Analys.C`: Macro to analyze data of simulations.

## Instructions
To run a simulation:

`$ ./generate_g3g4 -n 10 -j 2`

Where `-n` is the number of events and `-j` is the number of workers. This script will automatically run the `g3g4Analysis.C` macro. However, if you want to use it standalone, use the following command:

`root.exe -b -q g3g4Analysis.C`
