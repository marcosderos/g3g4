#!/bin/bash

workers=4;

while getopts "n:j:" opt; do
  case $opt in
    n) events=$OPTARG;;
    j) workers=$OPTARG;;
  esac
done

#shift 3;
#ir=$@;
echo "Number of events is : $events";
echo "workers: $workers";

mkdir g3 g4;
#geant3 sim
cd g3;
cp g3g4/MFTdictionary.bin .;
o2-sim -m PIPE ITS MFT ABS SHIL -e TGeant3 -n $events -g pythia8hi -j $workers;
o2-sim-digitizer-workflow -b --skipDet TPC,ITS,TOF,FT0,EMC,HMP,ZDC,TRD,MCH,MID,FDD,PHS,FV0,CPV;
o2-mft-reco-workflow -b;
cd ..;
#geant4 sim
cd g4;
cp g3g4/MFTdictionary.bin .;
o2-sim -m PIPE ITS MFT ABS SHIL -e TGeant4 -n $events -g pythia8hi -j $workers;
o2-sim-digitizer-workflow -b --skipDet TPC,ITS,TOF,FT0,EMC,HMP,ZDC,TRD,MCH,MID,FDD,PHS,FV0,CPV;
o2-mft-reco-workflow -b;
cd ..;

root.exe -b -q g3g4/g3g4Analysis.C;



